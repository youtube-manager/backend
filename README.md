# Youtube Video Manager: Backend

A REST API for Youtube Video Manager, that allows users to search for Youtube videos and add them to their's favourite video list.

## Deploy

To deploy a project do the following:

1. Execute `docker-compose -f deploy/docker-compose.yml build`
2. Execute `docker-compose -f deploy/docker-compose.yml up`
3. Visit [localhost](http://localhost "API URL")
4. Stop the containers (`docker-compose -f deploy/docker-compose.yml down`) at your finish.

## Testing

To run the tests do the following:

1. Execute `docker-compose -f deploy/docekr-compose.yml build`
2. Execute `docker-compose -f deploy/docker-compose.yml run django poetry run python3 manage.py test`
3. Stop the containers (`docker-compose -f deploy/docker-compose.yml down`) at your finish.

## Documentation

The Swagger-like documentation is able on the [application main page](http://localhost "API home URL").
