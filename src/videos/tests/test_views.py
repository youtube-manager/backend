from django.urls import path

from rest_framework.test import APITestCase, URLPatternsTestCase

from users.factories import AccountFactory

from videos.views import LikedVideoCreateAPIView



class TestLikedVideoCreateAPIView(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path('liked/', LikedVideoCreateAPIView.as_view())
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.account = AccountFactory()
        cls.url = '/liked/'
        cls.create_data = {
            'video_id': 'jadsjadsj',
            'user': cls.account.id,
            'data': {
                'title': 'Video title'
            }
        }

    def test_liked_video_create_api_view(self):
        self.client.force_authenticate(user=self.account)

        response = self.client.post(self.url, self.create_data, format='json')

        self.assertEqual(response.status_code, 201)

    def test_liked_video_create_api_view_invalid_user(self):
        another_account = AccountFactory(username='another')
        self.client.force_authenticate(user=another_account)

        response = self.client.post(self.url, self.create_data, format='json')

        self.assertEqual(response.status_code, 403)
