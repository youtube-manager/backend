from django.test import TestCase

from videos.formatters import VideoDataFormatter


class TestVideoDataFormatter(TestCase):
    def test_video_data_formatter(self):
        # arrange
        videos = [
            {
                'id': {
                    'videoId': 'rajjasjda'
                },
                'snippet': {
                    'title': '&quot;Title&quot;',
                    'thumbnails': [],
                    'publishedAt': '2012-02-12'
                }
            }
        ]

        # act
        formatted_videos = VideoDataFormatter.format(videos)
        formatted_video_data = formatted_videos[0]

        # assert
        self.assertTrue(isinstance(formatted_video_data['video_id'], str))
        self.assertEqual('"Title"', formatted_video_data['data']['title'])
