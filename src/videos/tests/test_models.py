from django.test import TestCase
from django.db.utils import IntegrityError

from users.factories import AccountFactory

from videos.models import LikedVideo


class TestLikedVideoModel(TestCase):
    def test_id_user_unique_together(self):
        account = AccountFactory()
        another_account = AccountFactory(username='unknown')
        video_id = 'adjsjasd'

        LikedVideo.objects.create(
            video_id=video_id,
            user=account,
            data={'title': 'title1'}
        )

        LikedVideo.objects.create(
            video_id=video_id,
            user=another_account,
            data={'title': 'title2'}
        )

        with self.assertRaises(IntegrityError) as error:
            LikedVideo.objects.create(
                video_id=video_id,
                user=account,
                data={'title': 'title2'}
            )
