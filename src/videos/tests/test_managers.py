from mock import patch

from django.test import TestCase

from users.factories import AccountFactory

from videos.models import SearchResult
from videos.managers import VideoManager


class TestVideoManager(TestCase):
    result = [
        {
            'id': {
                'videoId': 'rajjasjda'
            },
            'snippet': {
                'title': 'A python video',
                'thumbnails': [],
                'publishedAt': '2012-02-12'
            }
        }
    ]

    @patch.object(VideoManager, '_get_query_response', return_value=result)
    def test_video_manager_search(self, mock_method):
        # arrange
        video_manager = VideoManager()

        # act
        results = video_manager.search('python')

        # assert
        self.assertIsNotNone(results)
        self.assertIn('videos', results)
        self.assertTrue(isinstance(results['videos'], list))

    @patch.object(VideoManager, '_get_query_response', return_value=result)
    def test_video_manager_search_authenticated(self, mock_method):
        # arrange
        user = AccountFactory()
        video_manager = VideoManager()

        # act
        results = video_manager.search(query='python', user=user)

        # assert
        self.assertIsNotNone(results['videos'][0])
        self.assertIn('liked', results['videos'][0])

    def test_video_manager_search_saved_result(self):
        # arrange
        search_data = [
            {
                'video_id': 'asASDjasj',
                'data': {
                    'title': 'An IT video',
                }
            }
        ]
        SearchResult.objects.create(query='it', data=search_data)
        vm = VideoManager()

        # act
        search_result = vm.search('it').get('videos')
        # assert
        self.assertEqual(
            search_data[0]['video_id'],
            search_result[0]['video_id']
        )
