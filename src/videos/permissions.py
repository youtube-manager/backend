from rest_framework.permissions import BasePermission


class IsDataOwner(BasePermission):
    """Checks whether user is data owner

    If the object .user field equals to current user logged in then user, who
    send request - is data onwer.
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return obj.user.id == request.user.id


class CreateLikedVideoPermission(BasePermission):
    """
    Checks if user that tries to create a LikedVideo instance passes
    his own id through request.data
    """

    def has_permission(self, request, view):
        user = request.user
        # print(user.id, request.data['user'], '\n\n')
        return user.is_authenticated and user.id == request.data.get('user')

    def has_object_permission(self, request, view):
        return request.user.is_authenticated and obj.user == request.user
