class VideoDataFormatter:
    """Data formatter for Youtube Video data

    This class provides API for formating a list of received from Youtube API
    videos and gets only the needed information from it. It also formats data
    the way it is easy to store in database (packs it in JSON-like object).
    """

    @classmethod
    def format(cls, videos):
        """Formats the list of videos

        Args:
            videos (list): A list of received videos using Youtube API

        Returns:
            list: Formatted data
        """

        return [
            cls._format_video_data(video)
            for video in videos
        ]

    @classmethod
    def _format_video_data(cls, video):
        """Gets the youtube video data and pulls out the information needed
        from it.

        Args:
            video (dict): A video, received from Youtube API

        Returns:
            dict: Formatted video data
        """

        return {
            'video_id': video['id']['videoId'],
            'data': {
                'title': cls._format_video_title(video['snippet']['title']),
                'publishedAt': video['snippet']['publishedAt'],
                'thumbnails': video['snippet']['thumbnails']
            }
        }

    @classmethod
    def _format_video_title(cls, title):
        """Replaces HTML special symbols by it's unicode representation.

        Args:
            title (str): A video title

        Returns:
            str: A video title with replaced HTML special symbols
        """

        replacements = {
            '&quot;': '"',
            '&amp;': '&',
            '&gt;': '>',
            '&lt': '<',
            '&apos;': '\'',
            '&nbsp;': ' ',
            '&cent;': '¢',
            '&pound;': '£',
            '&yen;': '¥',
            '&euro;': '€',
            '&copy;': '©',
            '&reg;': '®'
        }

        for k, v in replacements.items():
            title = title.replace(k, v)

        return title
