from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from videos.managers import VideoManager
from videos.models import LikedVideo
from videos.permissions import IsDataOwner, CreateLikedVideoPermission
from videos.serializers import (
    SearchQuerySerializer,
    LikedVideoCreateSerializer
)


class SearchVideoAPIView(APIView):
    """
    A Django view for youtube video searching
    """

    def post(self, request):
        """
        Gets the search query with POST request params and returns the list of
        videos were found by given query.
        """

        serializer = SearchQuerySerializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        search_query = serializer.validated_data.get('query')
        video_manager = VideoManager()
        videos = video_manager.search(query=search_query, user=request.user)

        return Response(videos, status=status.HTTP_200_OK)


class LikedVideoCreateAPIView(CreateAPIView):
    """
    A Django view for adding videos to user's liked videos list.
    """

    permission_classes = (CreateLikedVideoPermission,)
    queryset = LikedVideo.objects.all()
    serializer_class = LikedVideoCreateSerializer


class LikedVideoDestroyAPIView(DestroyAPIView):
    """
    A Django view for removing video from user's liked list
    """

    permission_classes = (IsDataOwner,)

    def get_object(self):
        return get_object_or_404(
            LikedVideo,
            video_id=self.kwargs.get('video_id'),
            user=self.request.user
        )
