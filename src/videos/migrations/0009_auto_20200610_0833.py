# Generated by Django 3.0.7 on 2020-06-10 08:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0008_auto_20200605_1302'),
    ]

    operations = [
        migrations.AddField(
            model_name='likedvideo',
            name='video_id',
            field=models.CharField(default='rJdasjdasj', max_length=256),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='likedvideo',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
