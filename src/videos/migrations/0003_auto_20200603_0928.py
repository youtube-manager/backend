# Generated by Django 3.0.6 on 2020-06-03 09:28

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0002_auto_20200603_0722'),
    ]

    operations = [
        migrations.AddField(
            model_name='likedvideo',
            name='snippet',
            field=django.contrib.postgres.fields.jsonb.JSONField(default={}),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='likedvideo',
            name='thumbnails',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=[]),
            preserve_default=False,
        ),
    ]
