from django.conf import settings

from apiclient.discovery import build

from videos.models import SearchResult, LikedVideo

from videos.formatters import VideoDataFormatter


class VideoManager:
    """Youtube video data receiving manager

    This class provides API for searching youtube videos by given query.

    Attributes:
        PAGE_MAX_RESULTS (int): A value of results, got from Youtube API.
        youtube (Resource): A Python Google API object designed for interacting
            with Google API's
    """

    PAGE_MAX_RESULTS = 30
    youtube = build(
        'youtube', 'v3',
        developerKey=settings.YOUTUBE_API_KEY
    )

    def search(self, query, user=None, **query_kwargs):
        """Youtube video search method

        Searches videos with given query. If user was provided - inserts
        liked video state (boolean variable that shows whether video was liked
        by user or not).

        Args:
            query (str): A search query
            user (Account): A user that is currently looking for videos

        Returns:
            dict: A list of search retuls (`videos` dict key)
        """

        videos = self._get_videos(query, **query_kwargs)

        if user and user.is_authenticated:
            videos = self._insert_user_likes(videos, user)

        return {'videos': videos}

    def _get_videos(self, query, **query_kwargs):
        """Gets the searching videos list

        If such searching query were already given by any user - gets the
        results list from database. If not - sends request to youtube API
        and returns the formatted response.
        """

        search_result = SearchResult.objects.filter(query=query)

        if search_result.exists():
            return search_result.last().data

        videos = self._get_query_response(query, **query_kwargs)
        formated_videos = VideoDataFormatter.format(videos)

        self._save_search_result(query, formated_videos)
        return formated_videos

    def _save_search_result(self, query, videos):
        """
        Private method that saves search results to database.
        """

        return SearchResult.objects.create(query=query, data=videos)

    def _get_query_response(self, query, **query_kwargs):
        """
        Searches video by given query using the Google Python API. Returns the
        list of videos.
        """

        request = self.youtube.search().list(
            q=query,
            part='snippet',
            type='video',
            maxResults=self.PAGE_MAX_RESULTS
        )
        response = request.execute()
        return response['items']

    def _insert_user_likes(self, videos, user):
        """
        Inserts the video liked state that shows whether video was liked by
        user or not.
        """

        for video in videos:
            video['liked'] = LikedVideo.objects.filter(
                video_id=video['video_id'],
                user=user
            ).exists()

        return videos
