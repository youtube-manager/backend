from django.db import models
from django.contrib.postgres.fields import JSONField


class LikedVideo(models.Model):
    """Liked by user video model

    Attributes:
        video_id (str): videoId API parameter
        snipper (dict): Detail information about video
        user (Account): A user, who has liked the video
    """

    video_id = models.CharField(max_length=256)
    data = JSONField()
    user = models.ForeignKey(
        'users.Account',
        on_delete=models.CASCADE,
        related_name='liked_videos'
    )

    class Meta:
        unique_together = ('video_id', 'user')


class SearchResult(models.Model):
    """Search result model

    This model stores data about search requests that were made in past.

    Attributes:
        query (str): A search query, inputed by user in past
        data (list): A list of search results, banch of video details
    """

    query = models.CharField(max_length=256, unique=True)
    data = JSONField()
