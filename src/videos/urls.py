from django.urls import path

from videos.views import (
    SearchVideoAPIView,
    LikedVideoCreateAPIView,
    LikedVideoDestroyAPIView
)


app_name = 'videos'

urlpatterns = [
    path('search/', SearchVideoAPIView.as_view(), name='search'),
    path('liked/', LikedVideoCreateAPIView.as_view(), name='liked-create'),
    path(
        'liked/<str:video_id>/',
        LikedVideoDestroyAPIView.as_view(),
        name='liked-destroy'
    ),
]
