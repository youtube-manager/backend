from django.contrib import admin

from videos.models import SearchResult, LikedVideo


admin.site.register(SearchResult)
admin.site.register(LikedVideo)
