from rest_framework import serializers

from videos.models import LikedVideo


class SearchQuerySerializer(serializers.Serializer):
    query = serializers.CharField(max_length=256, required=True)


class LikedVideoListSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikedVideo
        fields = ('id', 'video_id', 'data')


class LikedVideoCreateSerializer(serializers.ModelSerializer):
   class Meta:
        model = LikedVideo
        fields = ('id', 'video_id', 'user', 'data')
