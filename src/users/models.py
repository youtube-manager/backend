from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from users.managers import AccountManager


class Account(AbstractBaseUser, PermissionsMixin):
    """Project user model

    Default model used for user authorization & authentication.

    Attributes:
        username (str): A user's name (login)
        password (str): A user's password
        objects (AccountManager): User model Django objects manager
        is_admin (bool): User being admin state
        is_active (bool): user being active (able for authorization) state
    """

    username = models.CharField(max_length=24, unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'

    objects = AccountManager()

    @property
    def is_staff(self):
        return self.is_admin
