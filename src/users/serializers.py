from rest_framework import serializers

from users.models import Account


class AccountCreateSerializer(serializers.ModelSerializer):
    """
    Serializer class for user creation. Gets the user name, password and
    password confirmation.
    """

    confirm_password = serializers.CharField(
        max_length=32,
        required=True,
        write_only=True
    )

    class Meta:
        model = Account
        fields = ('id', 'username', 'password', 'confirm_password')
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def validate(self, data):
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError({
                'confirm_password': 'Password confirmation do not match'
            })

        return super().validate(data)

    def create(self, validated_data):
        validated_data.pop('confirm_password')
        return Account.objects.create_user(**validated_data)


class AccountRetrieveSerializer(serializers.ModelSerializer):
    """
    Serializer class for retrieving (getting detail) user data.
    """

    class Meta:
        model = Account
        fields = ('id', 'username')
