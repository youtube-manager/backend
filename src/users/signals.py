from django.dispatch import receiver
from django.db.models.signals import post_save

from rest_framework.authtoken.models import Token

from users.models import Account


@receiver(post_save, sender=Account)
def on_account_create(instance=None, created=False, **kwawrgs):
    """
    Creates user authorization token on account creation
    """

    if created:
        Token.objects.create(user=instance)
