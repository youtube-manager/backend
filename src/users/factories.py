import factory


class AccountFactory(factory.django.DjangoModelFactory):
    """
    Default user model factory. Used for project testing
    """

    username = 'user'
    password = factory.PostGenerationMethodCall('set_password', 'querty123')

    class Meta:
        model = 'users.Account'
        django_get_or_create = ('username',)
