from django.test import TestCase

from users.serializers import AccountCreateSerializer


class TestAccountCreateSerializer(TestCase):
    def test_account_create_serializer_create(self):
        """
        Testing AccountCreateSerializer with valid data
        """

        # arrange
        create_data = {
            'username': 'user',
            'password': 'qwerty123',
            'confirm_password': 'qwerty123'
        }
        serializer = AccountCreateSerializer(data=create_data)

        # act
        is_valid = serializer.is_valid()
        instance = serializer.save()

        # assert
        self.assertTrue(is_valid)
        self.assertIsNotNone(instance.id)
        self.assertTrue(instance.check_password('qwerty123'))

    def test_account_create_serializer_create_invalid_confirm(self):
        """
        Testing AccountCraeteSerialzier with invalid confirm_password
        serializer field.
        """

        # arrange
        create_data = {
            'username': 'user',
            'password': 'qwerty123',
            'confirm_password': '123qwerty'
        }

        serializer = AccountCreateSerializer(data=create_data)

        # act
        is_valid = serializer.is_valid()

        # assert
        self.assertFalse(is_valid)
        self.assertIn('confirm_password', serializer.errors)
