from django.shortcuts import get_object_or_404

from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from videos.serializers import LikedVideoListSerializer

from users.models import Account
from users.serializers import (
    AccountCreateSerializer,
    AccountRetrieveSerializer
)


class AccountCreateAPIView(CreateAPIView):
    """
    A Django view that creates new user account
    """

    queryset = Account.objects.all()
    serializer_class = AccountCreateSerializer


class CurrentUserRetrieveAPIView(RetrieveAPIView):
    """
    A Django view that returns detail information about current user logged in
    """

    serializer_class = AccountRetrieveSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class UserLikedVideoListAPIView(ListAPIView):
    """
    A Django view that returns the list of user liked video
    """

    serializer_class = LikedVideoListSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        account = get_object_or_404(Account, pk=self.kwargs.get('pk'))
        return account.liked_videos.all()
