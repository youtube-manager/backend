from django.urls import path

from rest_framework.authtoken.views import obtain_auth_token

from users.views import (
    AccountCreateAPIView,
    CurrentUserRetrieveAPIView,
    UserLikedVideoListAPIView
)


app_name = 'users'


urlpatterns = [
    path('', AccountCreateAPIView.as_view(), name='account-create'),
    path(
        'current/',
        CurrentUserRetrieveAPIView.as_view(),
        name='retrieve-current-user'
    ),
    path(
        '<int:pk>/videos/liked/',
        UserLikedVideoListAPIView.as_view(),
        name='user-liked-list'
    ),
    path('login/', obtain_auth_token, name='login'),
]
